#!/bin/bash
apt-get update
apt-get install jq -y

# Set your GitLab repository URL and personal access token
GITLAB_URL="https://gitlab.com/khaddue1/pat_token_auto_rotation"
ACCESS_TOKEN="mytoken"

# Set the project ID (replace with the actual project ID)
CI_PROJECT_ID="54085180"

# Get the current date and time
CURRENT_DATE=$(date +"%Y%m%d%H%M%S")

# Generate a new personal access token
NEW_ACCESS_TOKEN=$(curl --header "PRIVATE-TOKEN: $ACCESS_TOKEN" "$GITLAB_URL/api/v4/user" | jq -r .id)

# Update the CI/CD variables in GitLab with the new token
curl --request PUT --header "PRIVATE-TOKEN: $ACCESS_TOKEN" "$GITLAB_URL/api/v4/projects/$CI_PROJECT_ID/variables" \
     --data-urlencode "key=CI_JOB_TOKEN" \
     --data-urlencode "value=$NEW_ACCESS_TOKEN" \
     --data-urlencode "variable_type=File" \
     --data-urlencode "protected=true" \
     --data-urlencode "masked=true" \
     --data-urlencode "environment_scope=*" \
     --data-urlencode "update_key=true"

# Print the new token (optional - you can remove this if you don't want to print the token)
echo "New Personal Access Token: $NEW_ACCESS_TOKEN"

# Clean up - remove the old token variable from CI/CD variables
curl --request DELETE --header "PRIVATE-TOKEN: $ACCESS_TOKEN" "$GITLAB_URL/api/v4/projects/$CI_PROJECT_ID/variables/CI_JOB_TOKEN"
